export class DestinoViaje {
    pais: string;
    ciudad: string;

    constructor(p:string, c:string){
        this.pais = p;
        this.ciudad = c;
    }
}